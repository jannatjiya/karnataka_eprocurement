from datetime import datetime
# hey there this is jannat
from selenium import webdriver
from selenium.webdriver.chrome.service import Service
import lxml.html
# "C:\Program Files\Google\Chrome\Application\chrome.exe"

from selenium.webdriver.support.ui import Select
from selenium.webdriver.common.by import By
import time
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC

# from selenium.webdriver.chrome.options import  Options
#
# chromeOptions = Options()
# chromeOptions.add_experimental_opstion("prefs",{"download.default_directory": "C:\Downloads"})



# driver = webdriver.Chrome(chrome_options=chromeOptions)
s = Service("C:/Users/hp/Downloads/chromedriver_win32 (6)/chromedriver.exe")
# s = Service("C:\Program Files\Google\Chrome\Application\chrome.exe")
# s = Service("C:\Users\hp\tender_internship\october_internship\tender_test\chromedriver.exe")

driver = webdriver.Chrome(service=s)
driver.maximize_window()

driver.get("https://eproc.karnataka.gov.in/")
driver.implicitly_wait(10)

eng = driver.find_element(By.ID, "englishid")
print(eng.is_displayed())
eng.click()

tender_button = driver.find_element(By.ID, "btnstyl")
tender_button.click()
driver.maximize_window()

print(driver.current_window_handle + "#this is our parent window")
handles = driver.window_handles  # returns all the handle values of opened browser windows

for handle in handles:
    driver.switch_to.window(handle)
    print(driver.title)

driver.maximize_window()





alltenders = {}
flag= True
previous_pg_first_sno_number=0
scroll_no=0
while flag== True:

    # driver.implicitly_wait(3)

    page_tree = lxml.html.document_fromstring(driver.page_source)
    list_of_rows = page_tree.xpath("//table/tbody[@id='eprocTenders:browserTableEprocTenders:tbody_element']/tr")

    fresh_pg_first_sno_number = page_tree.xpath(
        "//table/tbody[@id='eprocTenders:browserTableEprocTenders:tbody_element']/tr[1]/td[1]")
    fresh_pg_first_sno_number = fresh_pg_first_sno_number[0].text_content()

    if fresh_pg_first_sno_number== previous_pg_first_sno_number:
        print("it stop coz of this")
        flag=False
    else:
        pass

    previous_pg_first_sno_number = fresh_pg_first_sno_number

    for row in list_of_rows:
        sno = row.xpath("./td[1]")
        sno = int(sno[0].text_content())
        print("sno = " + str(sno))


        department = row.xpath(" ./td[2] ")
        dep = department[0].text_content().strip()
        print("dep no = " + str(dep))


        tender_number = row.xpath("./td[3]")
        tender_number = tender_number[0].text_content().strip()
        print("tender no = " + str(tender_number))


        tender_title = row.xpath("./td[4]")
        tender_title = tender_title[0].text_content().strip()
        print("tender title = " + str(tender_title))

        tender_type = row.xpath("./td[5]")
        tender_type = tender_type[0].text_content().strip()
        print("tender_type = " + str(tender_type))

        category = row.xpath("./td[6]")
        category = category[0].text_content().strip()
        print("category = " + str(category))

        sub_cateogry = row.xpath("./td[7]")
        sub_category = sub_cateogry[0].text_content().strip()
        print("sub_category = " + str(sub_category))

        estimated_value = row.xpath("./td[8]")
        try:
            estimated_value = float(estimated_value[0].text_content())
        except:
            estimated_value= None

        print("estimated value = " + str(estimated_value))

        nit_published_date = row.xpath("./td[9]")
        nit_published_date = nit_published_date[0].text_content()
        NIT_published_date = datetime.strptime(nit_published_date, '%d/%m/%Y %H:%M:%S')
        print("nit_published_date = " + str(NIT_published_date))


        last_date_for_bid_submission = row.xpath("./td[10]")
        last_date_for_bid_submission = last_date_for_bid_submission[0].text_content()
        last_date_for_Bid_submission = datetime.strptime(last_date_for_bid_submission, '%d/%m/%Y %H:%M:%S')
        print("last date for bid submission" + str(last_date_for_Bid_submission))


        tenderinfo = {
            "department": dep,
            "tender_no": tender_number,
            "tender_type": tender_type,
            "tender title": tender_title,
            "category": category,
            "sub_category": sub_category,
            "estimated_value": estimated_value,
            "NIT_date_for_submission": nit_published_date,
            "last_date_for_bid": last_date_for_bid_submission
        }

        print(tenderinfo)

        alltenders.update({tender_number: tenderinfo})

    next_button = driver.find_element(By.ID, "eprocTenders:dataScrollerIdnext")
    # next_button.click()







    for k in range(1,20):
        driver.implicitly_wait(2)

        if k != 1:
            for pg_scoll in range(0,scroll_no):
                next_button = driver.find_element(By.ID, "eprocTenders:dataScrollerIdnext")
                next_button.click()
        else:
            for pg_scoll in range(1,scroll_no):
                next_button = driver.find_element(By.ID, "eprocTenders:dataScrollerIdnext")
                next_button.click()

        search_button = driver.find_element_by_xpath("//table/tbody[@id='eprocTenders:browserTableEprocTenders:tbody_element']/tr["+str(k)+"]/td[11]/a[1]")
        search_button.click()




        detailed_page_tree = lxml.html.document_fromstring(driver.page_source)
        detail_body = detailed_page_tree.xpath("//span[@id='_id29:useMe']/table/tbody/tr/td/table[2]/tbody")
        len_setion1_row = len(detailed_page_tree.xpath("//span[@id='_id29:useMe']/table/tbody/tr/td/table[2]/tbody/tr/td[2]"))
        r13c1 = detailed_page_tree.xpath("//span[@id='_id29:useMe']/table/tbody/tr/td/table[2]/tbody/tr[14]/td[1]")
        r13c1 = r13c1[0].text_content()
        detailed_tender_info = {}
        general_conditions = {}


        tender_technical_qualification_criteria = {}
        document_evidence_required_from_the_bidder = {}
        work_item_details = {}
        technical_evaluation_criteria = {}
        bill_of_quantity = {}
        delivery_schedules = {}

        rest_data_details = {}
        download_section_details = {}
        tenderinfo = {}
        if r13c1 =="Highest Bidder Selection" :


            # higher_bidder_selection = detailed_page_tree.xpath(
            #     "//span[@id='_id29:useMe']/table/tbody/tr/td/table[2]/tbody/tr[14]/td[2]")
            # higher_bidder_selection = higher_bidder_selection[0].text_content()
            # if  higher_bidder_selection == "NO" :


            r1c1 = detailed_page_tree.xpath("//span[@id='_id29:useMe']/table/tbody/tr/td/table[2]/tbody/tr[2]/td[1]")
            r1c1 = r1c1[0].text_content()

            tender_no = detailed_page_tree.xpath("//span[@id='_id29:useMe']/table/tbody/tr/td/table[2]/tbody/tr[2]/td[2]")
            tender_no =tender_no[0].text_content()
            print(str(r1c1)+" = "+ str(tender_no))

            r2c1 = detailed_page_tree.xpath("//span[@id='_id29:useMe']/table/tbody/tr/td/table[2]/tbody/tr[3]/td[1]")
            r2c1 = r2c1[0].text_content()

            r2c2 = detailed_page_tree.xpath("//span[@id='_id29:useMe']/table/tbody/tr/td/table[2]/tbody/tr[3]/td[2]")
            r2c2 = r2c2[0].text_content()
            print(str(r2c1) + " = " + str(r2c2))

            r3c1 = detailed_page_tree.xpath("//span[@id='_id29:useMe']/table/tbody/tr/td/table[2]/tbody/tr[4]/td[1]")
            r3c1 = r3c1[0].text_content()

            r3c2 = detailed_page_tree.xpath("//span[@id='_id29:useMe']/table/tbody/tr/td/table[2]/tbody/tr[4]/td[2]")
            r3c2 = r3c2[0].text_content()
            print(str(r3c1) + " = " + str(r3c2))

            r4c1 = detailed_page_tree.xpath("//span[@id='_id29:useMe']/table/tbody/tr/td/table[2]/tbody/tr[5]/td[1]")
            r4c1 = r4c1[0].text_content()

            r4c2 = detailed_page_tree.xpath("//span[@id='_id29:useMe']/table/tbody/tr/td/table[2]/tbody/tr[5]/td[2]")
            r4c2 = r4c2[0].text_content()
            print(str(r4c1) + " = " + str(r4c2))

            r5c1 = detailed_page_tree.xpath("//span[@id='_id29:useMe']/table/tbody/tr/td/table[2]/tbody/tr[6]/td[1]")
            r5c1 = r5c1[0].text_content()

            r5c2 = detailed_page_tree.xpath("//span[@id='_id29:useMe']/table/tbody/tr/td/table[2]/tbody/tr[6]/td[2]")
            r5c2 = r5c2[0].text_content()
            print(str(r5c1) + " = " + str(r5c2))

            r6c1 = detailed_page_tree.xpath("//span[@id='_id29:useMe']/table/tbody/tr/td/table[2]/tbody/tr[7]/td[1]")
            r6c1 = r6c1[0].text_content()

            r6c2= detailed_page_tree.xpath("//span[@id='_id29:useMe']/table/tbody/tr/td/table[2]/tbody/tr[7]/td[2]")
            r6c2 = r6c2[0].text_content()
            print(str(r6c1) + " = " + str(r6c2))

            r7c1 = detailed_page_tree.xpath("//span[@id='_id29:useMe']/table/tbody/tr/td/table[2]/tbody/tr[8]/td[1]")
            r7c1 = r7c1[0].text_content()

            r7c2 = detailed_page_tree.xpath("//span[@id='_id29:useMe']/table/tbody/tr/td/table[2]/tbody/tr[8]/td[2]")
            r7c2 = r7c2[0].text_content()
            print(str(r7c1) + " = " + str(r7c2))

            r8c1 = detailed_page_tree.xpath("//span[@id='_id29:useMe']/table/tbody/tr/td/table[2]/tbody/tr[9]/td[1]")
            r8c1 = r8c1[0].text_content()

            r8c2 = detailed_page_tree.xpath("//span[@id='_id29:useMe']/table/tbody/tr/td/table[2]/tbody/tr[9]/td[2]")
            r8c2 = r8c2[0].text_content()
            print(str(r8c1) + " = " + str(r8c2))

            r9c1 = detailed_page_tree.xpath("//span[@id='_id29:useMe']/table/tbody/tr/td/table[2]/tbody/tr[10]/td[1]")
            r9c1 = r9c1[0].text_content()

            r9c2 = detailed_page_tree.xpath("//span[@id='_id29:useMe']/table/tbody/tr/td/table[2]/tbody/tr[10]/td[2]")
            r9c2 = r9c2[0].text_content()
            print(str(r9c1) + " = " + str(r9c2))

            r10c1 = detailed_page_tree.xpath("//span[@id='_id29:useMe']/table/tbody/tr/td/table[2]/tbody/tr[11]/td[1]")
            r10c1 = r10c1[0].text_content()

            r10c2 = detailed_page_tree.xpath("//span[@id='_id29:useMe']/table/tbody/tr/td/table[2]/tbody/tr[11]/td[2]")
            r10c2 = r10c2[0].text_content()
            print(str(r10c1) + " = " + str(r10c2))

            r11c1 = detailed_page_tree.xpath("//span[@id='_id29:useMe']/table/tbody/tr/td/table[2]/tbody/tr[12]/td[1]")
            r11c1 = r11c1[0].text_content()

            r11c2 = detailed_page_tree.xpath("//span[@id='_id29:useMe']/table/tbody/tr/td/table[2]/tbody/tr[12]/td[2]")
            r11c2 = r11c2[0].text_content()
            print(str(r11c1) + " = " + str(r11c2))

            r12c1 = detailed_page_tree.xpath("//span[@id='_id29:useMe']/table/tbody/tr/td/table[2]/tbody/tr[13]/td[1]")
            r12c1 = r12c1[0].text_content()

            r12c2 = detailed_page_tree.xpath("//span[@id='_id29:useMe']/table/tbody/tr/td/table[2]/tbody/tr[13]/td[2]")
            r12c2 = r12c2[0].text_content()
            print(str(r12c1) + " = " + str(r12c2))

            r13c1 = detailed_page_tree.xpath("//span[@id='_id29:useMe']/table/tbody/tr/td/table[2]/tbody/tr[14]/td[1]")
            r13c1 = r13c1[0].text_content()

            r13c2 = detailed_page_tree.xpath("//span[@id='_id29:useMe']/table/tbody/tr/td/table[2]/tbody/tr[14]/td[2]")
            r13c2 = r13c2[0].text_content()
            print(str(r13c1) + " = " + str(r13c2))

            detailed_tender_info = {
                r1c1 : tender_no,
                r2c1 : r2c2,
                r3c1 : r3c2,
                r4c1 : r4c2,
                r5c1 : r5c2,
                r6c1 : r6c2,
                r7c1 : r7c2,
                r8c1 : r8c2,
                r9c1 : r9c2,
                r10c1 : r10c2,
                r11c1 : r11c2,
                r12c1 : r12c2,
                r13c1 : r13c2

            }
            print("important information of tenders",detailed_tender_info)

            general_condition = detailed_page_tree.xpath("//span[ @ id = '_id29:useMe']/table/tbody/tr/td/table[2]/tbody/tr[16]/td/table/tbody/tr")
            for condition in general_condition:
                sno_of_condition = condition.xpath("./td[1]")
                sno_of_condition = int(sno_of_condition[0].text_content())
                # print("sno_of_condition = " + str(sno_of_condition))

                condition_for_eligibility = condition.xpath("./td[2]")
                condition_for_eligibility = condition_for_eligibility[0].text_content()
                # print("condition_for_eligibility = " + str(condition_for_eligibility))

                condition_for_eligibility_tenders = {
                    "sno": sno_of_condition,
                    "condition": condition_for_eligibility

                }
                # print(condition_for_eligibility_tenders)
                general_conditions.update({sno_of_condition : condition_for_eligibility_tenders})
            try:
                # print("general condition dict is",general_conditions)
                print()


            except:
                print("the general condition dict is not printed")

            section3 = detailed_page_tree.xpath( "//span[ @ id = '_id29:useMe']/table/tbody/tr/td/table[2]/tbody/tr[17]")

            section3 = section3[0].text_content()
            # print("section3 name is",section3)
            section3heading = detailed_page_tree.xpath( "//span[ @ id = '_id29:useMe']/table/tbody/tr/td/table[2]/tbody/tr[18]/td/table/thead/tr")
            section3body = detailed_page_tree.xpath( "//span[ @ id = '_id29:useMe']/table/tbody/tr/td/table[2]/tbody/tr[18]/td/table/tbody/tr")

            s3={}
            section3_data = {}
            for sh in section3heading:
                s3h1 = sh.xpath("./th[1]")
                s3h1 = s3h1[0].text_content().strip()
                # print("detailed section3 heading col1",s3h1)

                s3h2 = sh.xpath("./th[2]")
                s3h2 = s3h2[0].text_content().strip()
                # print("detailed section3 heading col1",s3h2)

                try:
                    s3h3 = sh.xpath("./th[3]")
                    s3h3 = s3h3[0].text_content().strip()
                    # print("detailed section3 heading col1",s3h3)
                except:
                    pass

                try:
                    s3h4 = sh.xpath("./th[4]")
                    s3h4 = s3h4[0].text_content().strip()
                    # print("detailed section3 heading col1",s3h4)

                except:
                    pass
            # print("section3")

            for s3b in section3body:
                s3b1 = s3b.xpath("./td[1]")
                s3b1 = s3b1[0].text_content().strip()
                # print("detailed section3 heading col1",s3b1)

                s3b2 = s3b.xpath("./td[2]")
                s3b2 = s3b2[0].text_content().strip()
                # print("detailed section3 heading col1",s3b2)


                try:
                    s3b3 = s3b.xpath("./td[3]")
                    s3b3 = s3b3[0].text_content().strip()
                    # print("detailed section3 heading col1",s3b3)
                except:
                    pass

                try:
                    s3b4 = s3b.xpath("./td[4]")
                    s3b4 = s3b4[0].text_content().strip()
                    # print("detailed section3 heading col1",s3b4)

                except:
                    pass

                try:
                    try:
                        s3 = {
                            s3h1: s3b1,
                            s3h2: s3b2,
                            s3h3: s3b3,
                            s3h3: s3b3
                        }
                    except:

                        s3 = {
                            s3h1: s3b1,
                            s3h2: s3b2,
                            s3h3: s3b3,
                        }
                except:

                    s3 = {
                        s3h1: s3b1,
                        s3h2: s3b2,

                    }

                section3_data.update({s3b1: s3})
                if section3=="Tender Technical Qualification Criteria":
                    tender_technical_qualification_criteria = section3_data.copy()
                    # print("Tender Technical Qualification Criteria:", tender_technical_qualification_criteria)

                elif section3== "Document/Evidence Required from the Bidder":
                    document_evidence_required_from_the_bidder = section3_data.copy()
                    # print("Document/Evidence Required from the Bidder :",document_evidence_required_from_the_bidder)

                elif section3== "Work Item Details":
                    work_item_details = section3_data.copy()
                    # print("Work Item Details:",work_item_details)

                elif section3 == "Technical Evaluation Criteria":
                    technical_evaluation_criteria = section3_data.copy()
                    # print("Technical Evaluation Criteria:",technical_evaluation_criteria)

                elif section3 == "Bill Of Quantity":
                    bill_of_quantity = section3_data.copy()
                    # print("Bill Of Quantity :",bill_of_quantity)

                elif section3 == "Delivery Schedules":
                    delivery_schedules = section3_data.copy()
                    # print("Delivery Schedules:", delivery_schedules)

                else:
                    pass


                # print("this is s3 details", s3)
            # print("section3 information",section3_data)
            # print("section3body")


            #here is the  copy area
            try:
                s4th = {}
                section4_data = {}
                # print("execute section4 exist")
                section4 = detailed_page_tree.xpath("//span[ @ id = '_id29:useMe']/table/tbody/tr/td/table[2]/tbody/tr[19]")
                section4 = section4[0].text_content().strip()
                # print("section4 name is", section4)
                section4heading = detailed_page_tree.xpath(
                    "//span[ @ id = '_id29:useMe']/table/tbody/tr/td/table[2]/tbody/tr[20]/td/table/thead/tr")
                section4body = detailed_page_tree.xpath(
                    "//span[ @ id = '_id29:useMe']/table/tbody/tr/td/table[2]/tbody/tr[20]/td/table/tbody/tr")

                for s4h in section4heading:
                    s4h1 = s4h.xpath("./th[1]")
                    s4h1 = s4h1[0].text_content().strip()
                    # print("detailed section4 heading col1", s4h1)

                    s4h2 = s4h.xpath("./th[2]")
                    s4h2 = s4h2[0].text_content().strip()
                    # print("detailed section4 heading col2", s4h2)

                    try:
                        s4h3 = s4h.xpath("./th[3]")
                        s4h3 = s4h3[0].text_content().strip()
                        # print("detailed section4 heading col3", s4h3)
                    except:
                        pass

                    try:
                        s4h4 = s4h.xpath("./th[4]")
                        s4h4 = s4h4[0].text_content().strip()
                        # print("detailed section4 heading col4", s4h4)

                    except:
                        pass

                # print("section4")

                for s4b in section4body:
                    s4b1 = s4b.xpath("./td[1]")
                    s4b1 = s4b1[0].text_content().strip()
                    # print("detailed section4 body col1", s4b1)

                    s4b2 = s4b.xpath("./td[2]")
                    s4b2 = s4b2[0].text_content().strip()
                    # print("detailed section4 body col2", s4b2)

                    try:
                        s4b3 = s4b.xpath("./td[3]")
                        s4b3 = s4b3[0].text_content().strip()
                        # print("detailed section4 body col3", s4b3)
                    except:
                        pass

                    try:
                        s4b4 = s4b.xpath("./td[4]")
                        s4b4 = s4b4[0].text_content().strip()
                        # print("detailed section4 body col4", s4b4)

                    except:
                        pass
                    try:
                        try:
                            s4th = {
                                s4h1: s4b1,
                                s4h2: s4b2,
                                s4h3: s4b3,
                                s4h4: s4b4
                            }
                        except:

                            s4th = {
                                s4h1: s4b1,
                                s4h2: s4b2,
                                s4h3: s4b3,
                            }
                    except:

                        s4th = {
                            s4h1: s4b1,
                            s4h2: s4b2,

                        }

                    section4_data.update({s4b1: s4th})
                    # print("this is s4 dict", s4th)
                    if section4 == "Tender Technical Qualification Criteria":
                        tender_technical_qualification_criteria = section4_data.copy()
                        # print("Tender Technical Qualification Criteria:", tender_technical_qualification_criteria)

                    elif section4 == "Document/Evidence Required from the Bidder":
                        document_evidence_required_from_the_bidder = section4_data.copy()
                        # print("Document/Evidence Required from the Bidder :", document_evidence_required_from_the_bidder)


                    elif section4 == "Work Item Details":
                        work_item_details = section4_data.copy()
                        # print("Work Item Details:", work_item_details)

                    elif section4 == "Technical Evaluation Criteria":
                        technical_evaluation_criteria = section4_data.copy()
                        # print("Technical Evaluation Criteria :", technical_evaluation_criteria)

                    elif section4 == "Bill Of Quantity":
                        bill_of_quantity = section4_data.copy()
                        # print("Bill Of Quantity :", bill_of_quantity)

                    elif section4 == "Delivery Schedules":
                        delivery_schedules = section4_data.copy()
                        # print("Delivery Schedules:", delivery_schedules)

                    else:
                        pass
                # print("this is all detail of section4", s4th)

                try:
                    s5={}
                    section5={}
                    # print("execute section4 and 5 exist")
                    section5 = detailed_page_tree.xpath(
                        "//span[ @ id = '_id29:useMe']/table/tbody/tr/td/table[2]/tbody/tr[21]")
                    section5 = section5[0].text_content.strip()
                    # print("section5 name is", section5)
                    section5heading = detailed_page_tree.xpath(
                        "//span[ @ id = '_id29:useMe']/table/tbody/tr/td/table[2]/tbody/tr[22]/td/table/thead/tr")
                    section5body = detailed_page_tree.xpath(
                        "//span[ @ id = '_id29:useMe']/table/tbody/tr/td/table[2]/tbody/tr[22]/td/table/tbody/tr")

                    for s5h in section5heading:
                        s5h1 = s5h.xpath("./th[1]")
                        s5h1 = s5h1[0].text_content().strip()
                        # print("detailed section5 heading col1", s5h1)

                        s5h2 = s5h.xpath("./th[2]")
                        s5h2 = s5h2[0].text_content().strip()

                        # print("detailed section5 heading col2", s5h2)

                        try:
                            s5h3 = s5h.xpath("./th[3]")
                            s5h3 = s5h3[0].text_content().strip()
                            # print("detailed section5 heading col3", s5h3)
                        except:
                            pass

                        try:
                            s5h4 = s5h.xpath("./th[4]")
                            s5h4 = s5h4[0].text_content().strip()
                            # print("detailed section5 heading col4", s5h4)

                        except:
                            pass

                    print("section5")

                    for s5b in section5body:
                        s5b1 = s5b.xpath("./td[1]")
                        s5b1 = s5b1[0].text_content().strip()
                        # print("detailed section5 body col1", s5b1)

                        s5b2 = s5b.xpath("./td[2]")
                        s5b2 = s5b2[0].text_content().strip()
                        # print("detailed section5 body col2", s5b2)

                        try:
                            s5b3 = s5b.xpath("./td[3]")
                            s5b3 = s5b3[0].text_content().strip()
                            # print("detailed section5 body col3", s5b3)
                        except:
                            pass

                        try:
                            s5b4 = s5b.xpath("./td[4]")
                            s5b4 = s5b4[0].text_content().strip()
                            # print("detailed section5 body col4", s5b4)

                        except:
                            pass

                        try:
                            s5b5 = s5b.xpath("./td[4]")
                            s5b5 = s5b5[0].text_content().strip()
                            # print("detailed section5 body col4", s5b4)

                        except:
                            pass
                        try:
                            try:
                                s5 = {
                                    s5h1: s5b1,
                                    s5h2: s5b2,
                                    s5h3: s5b3,
                                    s5h4: s5b4
                                }
                            except:
                                s5 = {
                                    s5h1: s5b1,
                                    s5h2: s5b2,
                                    s5h3: s5b3, }
                        except:
                            s5 = {
                                s5h1: s5b1,
                                s5h2: s5b2,
                            }
                        section5_data.update({s5h1 : s5})
                        # print("this is s5 dict", s5)
                        if section5 == "Tender Technical Qualification Criteria":
                            tender_technical_qualification_criteria = section5_data.copy()
                            # print("Tender Technical Qualification Criteria:", tender_technical_qualification_criteria)

                        elif section5 == "Document/Evidence Required from the Bidder":
                            document_evidence_required_from_the_bidder = section5_data.copy()
                            # print("Document/Evidence Required from the Bidder :",
                            #       document_evidence_required_from_the_bidder)


                        elif section5 == "Work Item Details":
                            work_item_details = section5_data.copy()
                            # print("Work Item Details:", work_item_details)

                        elif section5 == "Technical Evaluation Criteria":
                            technical_evaluation_criteria = section5_data.copy()
                            # print("Technical Evaluation Criteria :", technical_evaluation_criteria)

                        elif section5 == "Bill Of Quantity":
                            bill_of_quantity = section5_data.copy()
                            # print("Bill Of Quantity :", bill_of_quantity)

                        elif section5 == "Delivery Schedules":
                            delivery_schedules = section5_data.copy()
                            # print("Delivery Schedules:", delivery_schedules)

                        else:
                            pass
                    # print("section5 all data" ,section5_data)


                    try:
                        s6={}
                        section6_data = {}
                        # print("execute section4,5,6 exist")
                        section6 = detailed_page_tree.xpath(
                            "//span[ @ id = '_id29:useMe']/table/tbody/tr/td/table[2]/tbody/tr[23]")
                        section6 = section6[0].text_content().strip()
                        # print("section6 name is", section6)
                        section6heading = detailed_page_tree.xpath(
                            "//span[ @ id = '_id29:useMe']/table/tbody/tr/td/table[2]/tbody/tr[24]/td/table/thead/tr")
                        section6body = detailed_page_tree.xpath(
                            "//span[ @ id = '_id29:useMe']/table/tbody/tr/td/table[2]/tbody/tr[24]/td/table/tbody/tr")

                        for s6h in section6heading:
                            s6h1 = s6h.xpath("./th[1]")
                            s6h1 = s6h1[0].text_content().strip()
                            # print("detailed section6 heading col1", s6h1)

                            s6h2 = s6h.xpath("./th[2]")
                            s6h2 = s6h2[0].text_content().strip()
                            # print("detailed section6 heading col2", s6h2)

                            try:
                                s6h3 = s6h.xpath("./th[3]")
                                s6h3 = s6h3[0].text_content().strip()
                                # print("detailed section6 heading col3", s6h3)
                            except:
                                pass

                            try:
                                s6h4 = s6h.xpath("./th[4]")
                                s6h4 = s6h4[0].text_content().strip()
                                # print("detailed section6 heading col4", s6h4)

                            except:
                                pass
                        print("section6")

                        for s6b in section6body:
                            s6b1 = s6b.xpath("./td[1]")
                            s6b1 = s6b1[0].text_content().strip()
                            # print("detailed section6 body col1", s6b1)

                            s6b2 = s6b.xpath("./td[2]")
                            s6b2 = s6b2[0].text_content().strip()
                            # print("detailed section6 body col2", s6b2)

                            try:
                                s6b3 = s6b.xpath("./td[3]")
                                s6b3 = s6b3[0].text_content().strip()
                                # print("detailed section6 body col3", s6b3)
                            except:
                                pass

                            try:
                                s6b4 = s6b.xpath("./td[4]")
                                s6b4 = s6b4[0].text_content().strip()
                                # print("detailed section6 body col4", s6b4)

                            except:
                                pass

                            try:
                                s6b5 = s6b.xpath("./td[4]")
                                s6b5 = s6b5[0].text_content().strip()
                                # print("detailed section6 body col4", s6b4)

                            except:
                                pass
                            try:
                                try:
                                    s6 = {
                                        s6h1: s6b1,
                                        s6h2: s6b2,
                                        s6h3: s6b3,
                                        s6h4: s6b4
                                    }
                                except:
                                    s6 = {
                                        s6h1: s6b1,
                                        s6h2: s6b2,
                                        s6h3: s6b3}
                            except:
                                s6 = {
                                    s6h1: s6b1,
                                    s6h2: s6b2}
                            print(s6b1, s6)

                            section6_data.update({"section6": s6})
                            if section6 == "Tender Technical Qualification Criteria":
                                tender_technical_qualification_criteria = section6_data.copy()
                                # print("Tender Technical Qualification Criteria:", tender_technical_qualification_criteria)

                            elif section6 == "Document/Evidence Required from the Bidder":
                                document_evidence_required_from_the_bidder = section6_data.copy()
                                # print("Document/Evidence Required from the Bidder :", document_evidence_required_from_the_bidder)


                            elif section6 == "Work Item Details":
                                work_item_details = section6_data.copy()
                                # print("Work Item Details:", work_item_details)

                            elif section6 == "Technical Evaluation Criteria":
                                technical_evaluation_criteria = section6_data.copy()
                                # print("Technical Evaluation Criteria :", technical_evaluation_criteria)

                            elif section6 == "Bill Of Quantity":
                                bill_of_quantity = section6_data.copy()
                                # print("Bill Of Quantity :", bill_of_quantity)

                            elif section6 == "Delivery Schedules":
                                delivery_schedules = section3_data.copy()
                                # print("Delivery Schedules:", delivery_schedules)

                            else:
                                pass

                        # print("section6 data",section6_data)



                        for i in range(19, 43):
                            count=count+1
                            try:

                                try:
                                    col1 = detailed_page_tree.xpath(
                                        "//span[@id='_id29:useMe']/table/tbody/tr/td/table[2]/tbody/tr[" + str(i) + "]/td[1]")
                                    col1 = col1[0].text_content().strip()
                                    # print("row number is ", i, "col1 = ", col1)

                                    col2 = detailed_page_tree.xpath(
                                        "//span[@id='_id29:useMe']/table/tbody/tr/td/table[2]/tbody/tr[" + str(i) + "]/td[2]")
                                    col2 = col2[0].text_content().strip()
                                    # print("row number is ", i, "col2 = ", col2)
                                    try:
                                        rest_information={
                                            col1 : col2
                                        }
                                        # print("other details are",rest_information)
                                        rest_data_details.update({col1 : rest_information})
                                    except:
                                        pass

                                except:
                                    pass
                            except:
                                pass
                        print("rest info is",rest_data_details)
                        # back_link = driver.find_element_by_name('_id29:butBack')
                        # back_link.click()
                    except:
                        # print("execute section4,5 exist and 6th doesnt")


                        for i in range(19, 43):
                            try:
                                try:

                                    col1 = detailed_page_tree.xpath(
                                        "//span[@id='_id29:useMe']/table/tbody/tr/td/table[2]/tbody/tr[" + str(i) + "]/td[1]")
                                    col1 = col1[0].text_content().strip()
                                    #print("row number is ", i, "col1 = ", col1)

                                    col2 = detailed_page_tree.xpath(
                                        "//span[@id='_id29:useMe']/table/tbody/tr/td/table[2]/tbody/tr[" + str(i) + "]/td[2]")
                                    col2 = col2[0].text_content().strip()
                                    #print("row number is ", i, "col2 = ", col2)
                                    try:
                                        rest_information = {
                                            col1: col2
                                        }
                                        # print("other details are", rest_information)

                                        rest_data_details.update({col1 : rest_information})
                                    except:
                                        pass
                                except:
                                    pass

                            except:
                                pass

                        print("rest info is", rest_data_details)

                except:
                    # print("execute section4 exist and 5,6 doesn't")

                    for i in range(19, 43):
                        try:
                            try:


                                col1 = detailed_page_tree.xpath(
                                    "//span[@id='_id29:useMe']/table/tbody/tr/td/table[2]/tbody/tr[" + str(i) + "]/td[1]")
                                col1 = col1[0].text_content().strip()
                                # print("row number is ", i, "col1 = ", col1)

                                col2 = detailed_page_tree.xpath(
                                    "//span[@id='_id29:useMe']/table/tbody/tr/td/table[2]/tbody/tr[" + str(i) + "]/td[2]")
                                col2 = col2[0].text_content().strip()
                                # print("row number is ", i, "col2 = ", col2)
                                try:

                                    rest_information = {
                                        col1: col2
                                    }
                                    # print("other details are", rest_information)
                                    rest_data_details.update({col1 : rest_information})
                                except:
                                    pass
                            except:
                                pass
                        except:
                            pass
                    print("rest info is", rest_data_details)





                # EXCEPT SECTION4 NOT EXIST
            except:

                # print("execute section 4,5,6 doesn't exist")

                for i in range(19, 43):
                    try:
                        try:

                            col1 = detailed_page_tree.xpath(
                                "//span[@id='_id29:useMe']/table/tbody/tr/td/table[2]/tbody/tr[" + str(i) + "]/td[1]")
                            col1 = col1[0].text_content().strip()
                            # print("row number is ", i, "col1 = ", col1)

                            col2 = detailed_page_tree.xpath(
                                "//span[@id='_id29:useMe']/table/tbody/tr/td/table[2]/tbody/tr[" + str(i) + "]/td[2]")
                            col2 = col2[0].text_content().strip()
                            # print("row number is ", i, "col2 = ", col2)
                            try:
                                rest_information = {
                                    col1: col2
                                }
                                # print("other details are", rest_information)
                                rest_data_details.update({col1 : rest_information})
                            except:
                                pass
                        except:
                            pass

                    except:
                            pass

                print("rest info is", rest_data_details)

            print("tender detail of general conditions = ", general_conditions)
            print("tender detail of  work item = ", document_evidence_required_from_the_bidder)
            print("tender detail of  tender_technical_qualification_criteria = ", tender_technical_qualification_criteria)
            print("tender detail of  document_evidence_required_from_the_bidder = ", document_evidence_required_from_the_bidder)
            print("tender detail of  technical_evaluation_criteria = ", technical_evaluation_criteria)
            print("tender detail of   bill_of_quantity = ", bill_of_quantity)
            print("tender detail of   delivery_schedules = ", delivery_schedules)

            print("all the other info of tender = ", rest_data_details)

            other_info_of_tender = {"other_info_of_tender": rest_data_details}
            general_conditions_data = {"general_conditions": general_conditions}

            work_item_details_data = {"work_item": work_item_details}
            tender_technical_qualification_criteria_data = {
                "tender_technical_qualification_criteria": tender_technical_qualification_criteria}
            technical_evaluation_criteria_data = {"technical_evaluation_criteria": technical_evaluation_criteria}
            bill_of_quantity_data = {"bill_of_quantity": bill_of_quantity}
            delivery_schedules_data = {"delivery_schedules": delivery_schedules}
            document_evidence_required_from_the_bidder_data = {
                "document_evidence_required_from_the_bidder": document_evidence_required_from_the_bidder}
            tender_data = {
                tender_no: (detailed_tender_info, general_conditions_data, tender_technical_qualification_criteria_data,
                            document_evidence_required_from_the_bidder_data, work_item_details_data,
                            technical_evaluation_criteria_data,
                            bill_of_quantity_data, delivery_schedules_data, other_info_of_tender)}

            alltenders.update({tender_no: tender_data})
            print("/n")
            print("complete detail of the tender", tender_data)

            back_link = driver.find_element_by_name('_id29:butBack')
            back_link.click()
            driver.implicitly_wait(10)

        else:

            #this is else loop



            detailed_page_tree = lxml.html.document_fromstring(driver.page_source)
            detail_body = detailed_page_tree.xpath("//span[@id='_id29:useMe']/table/tbody/tr/td/table[2]/tbody")
            # print("elif condition check")
            r1c1 = detailed_page_tree.xpath("//span[@id='_id29:useMe']/table/tbody/tr/td/table[2]/tbody/tr[2]/td[1]")
            r1c1 = r1c1[0].text_content().strip()

            tender_no = detailed_page_tree.xpath("//span[@id='_id29:useMe']/table/tbody/tr/td/table[2]/tbody/tr[2]/td[2]")
            tender_no = tender_no[0].text_content().strip()
            print(str(r1c1) + " = " + str(tender_no))

            r2c1 = detailed_page_tree.xpath("//span[@id='_id29:useMe']/table/tbody/tr/td/table[2]/tbody/tr[3]/td[1]")
            r2c1 = r2c1[0].text_content().strip()

            r2c2 = detailed_page_tree.xpath("//span[@id='_id29:useMe']/table/tbody/tr/td/table[2]/tbody/tr[3]/td[2]")
            r2c2 = r2c2[0].text_content().strip()
            print(str(r2c1) + " = " + str(r2c2))

            r3c1 = detailed_page_tree.xpath("//span[@id='_id29:useMe']/table/tbody/tr/td/table[2]/tbody/tr[4]/td[1]")
            r3c1 = r3c1[0].text_content().strip()

            r3c2 = detailed_page_tree.xpath("//span[@id='_id29:useMe']/table/tbody/tr/td/table[2]/tbody/tr[4]/td[2]")
            r3c2 = r3c2[0].text_content().strip()
            print(str(r3c1) + " = " + str(r3c2))

            r4c1 = detailed_page_tree.xpath("//span[@id='_id29:useMe']/table/tbody/tr/td/table[2]/tbody/tr[5]/td[1]")
            r4c1 = r4c1[0].text_content().strip()

            r4c2 = detailed_page_tree.xpath("//span[@id='_id29:useMe']/table/tbody/tr/td/table[2]/tbody/tr[5]/td[2]")
            r4c2 = r4c2[0].text_content().strip()
            print(str(r4c1) + " = " + str(r4c2))

            r5c1 = detailed_page_tree.xpath("//span[@id='_id29:useMe']/table/tbody/tr/td/table[2]/tbody/tr[6]/td[1]")
            r5c1 = r5c1[0].text_content().strip()

            r5c2 = detailed_page_tree.xpath("//span[@id='_id29:useMe']/table/tbody/tr/td/table[2]/tbody/tr[6]/td[2]")
            r5c2 = r5c2[0].text_content().strip()
            print(str(r5c1) + " = " + str(r5c2))

            r6c1 = detailed_page_tree.xpath("//span[@id='_id29:useMe']/table/tbody/tr/td/table[2]/tbody/tr[7]/td[1]")
            r6c1 = r6c1[0].text_content().strip()

            r6c2 = detailed_page_tree.xpath("//span[@id='_id29:useMe']/table/tbody/tr/td/table[2]/tbody/tr[7]/td[2]")
            r6c2 = r6c2[0].text_content().strip()
            print(str(r6c1) + " = " + str(r6c2))

            r7c1 = detailed_page_tree.xpath("//span[@id='_id29:useMe']/table/tbody/tr/td/table[2]/tbody/tr[8]/td[1]")
            r7c1 = r7c1[0].text_content().strip()

            r7c2 = detailed_page_tree.xpath("//span[@id='_id29:useMe']/table/tbody/tr/td/table[2]/tbody/tr[8]/td[2]")
            r7c2 = r7c2[0].text_content().strip()
            print(str(r7c1) + " = " + str(r7c2))

            r8c1 = detailed_page_tree.xpath("//span[@id='_id29:useMe']/table/tbody/tr/td/table[2]/tbody/tr[9]/td[1]")
            r8c1 = r8c1[0].text_content().strip()

            r8c2 = detailed_page_tree.xpath("//span[@id='_id29:useMe']/table/tbody/tr/td/table[2]/tbody/tr[9]/td[2]")
            r8c2 = r8c2[0].text_content().strip()
            print(str(r8c1) + " = " + str(r8c2))

            r9c1 = detailed_page_tree.xpath("//span[@id='_id29:useMe']/table/tbody/tr/td/table[2]/tbody/tr[10]/td[1]")
            r9c1 = r9c1[0].text_content().strip()

            r9c2 = detailed_page_tree.xpath("//span[@id='_id29:useMe']/table/tbody/tr/td/table[2]/tbody/tr[10]/td[2]")
            r9c2 = r9c2[0].text_content().strip()
            print(str(r9c1) + " = " + str(r9c2))

            r10c1 = detailed_page_tree.xpath("//span[@id='_id29:useMe']/table/tbody/tr/td/table[2]/tbody/tr[11]/td[1]")
            r10c1 = r10c1[0].text_content().strip()

            r10c2 = detailed_page_tree.xpath("//span[@id='_id29:useMe']/table/tbody/tr/td/table[2]/tbody/tr[11]/td[2]")
            r10c2 = r10c2[0].text_content().strip()
            print(str(r10c1) + " = " + str(r10c2))

            r11c1 = detailed_page_tree.xpath("//span[@id='_id29:useMe']/table/tbody/tr/td/table[2]/tbody/tr[12]/td[1]")
            r11c1 = r11c1[0].text_content().strip()

            r11c2 = detailed_page_tree.xpath("//span[@id='_id29:useMe']/table/tbody/tr/td/table[2]/tbody/tr[12]/td[2]")
            r11c2 = r11c2[0].text_content().strip()
            print(str(r11c1) + " = " + str(r11c2))

            r12c1 = detailed_page_tree.xpath("//span[@id='_id29:useMe']/table/tbody/tr/td/table[2]/tbody/tr[13]/td[1]")
            r12c1 = r12c1[0].text_content().strip()

            r12c2 = detailed_page_tree.xpath("//span[@id='_id29:useMe']/table/tbody/tr/td/table[2]/tbody/tr[13]/td[2]")
            r12c2 = r12c2[0].text_content().strip()
            print(str(r12c1) + " = " + str(r12c2))



            detailed_tender_info = {
                r1c1: tender_no,
                r2c1: r2c2,
                r3c1: r3c2,
                r4c1: r4c2,
                r5c1: r5c2,
                r6c1: r6c2,
                r7c1: r7c2,
                r8c1: r8c2,
                r9c1: r9c2,
                r10c1: r10c2,
                r11c1: r11c2,
                r12c1: r12c2


            }
            print("detailed data of tender",detailed_tender_info)
            driver.implicitly_wait(10)


            general_condition = detailed_page_tree.xpath(
                "//span[ @ id = '_id29:useMe']/table/tbody/tr/td/table[2]/tbody/tr[15]/td/table/tbody/tr")
            for condition in general_condition:
                sno_of_condition = condition.xpath("./td[1]")
                sno_of_condition = int(sno_of_condition[0].text_content().strip())
                # print("sno_of_condition = " + str(sno_of_condition))

                condition_for_eligibility = condition.xpath("./td[2]")
                condition_for_eligibility = condition_for_eligibility[0].text_content().strip()
                # print("condition_for_eligibility = " + str(condition_for_eligibility))

                condition_for_eligibility_tenders = {
                    "sno": sno_of_condition,
                    "condition": condition_for_eligibility

                }
                # print(condition_for_eligibility_tenders)
                general_conditions.update({sno_of_condition: condition_for_eligibility_tenders })
            # print("hppy")
            # print("general conditions are",general_conditions)

            s3={}
            section3_data={}
            section3 = detailed_page_tree.xpath("//span[ @ id = '_id29:useMe']/table/tbody/tr/td/table[2]/tbody/tr[16]")
            section3 = section3[0].text_content().strip()
            # print("section3 name is", section3)
            section3heading = detailed_page_tree.xpath(
                "//span[ @ id = '_id29:useMe']/table/tbody/tr/td/table[2]/tbody/tr[17]/td/table/thead/tr")
            section3body = detailed_page_tree.xpath(
                "//span[ @ id = '_id29:useMe']/table/tbody/tr/td/table[2]/tbody/tr[17]/td/table/tbody/tr")

            for sh in section3heading:
                s3h1 = sh.xpath("./th[1]")
                s3h1 = s3h1[0].text_content().strip()
                # print("detailed section3 heading col1", s3h1)

                s3h2 = sh.xpath("./th[2]")
                s3h2 = s3h2[0].text_content().strip()
                # print("detailed section3 heading col1", s3h2)

                try:
                    s3h3 = sh.xpath("./th[3]")
                    s3h3 = s3h3[0].text_content().strip()
                    # print("detailed section3 heading col1", s3h3)
                except:
                    pass

                try:
                    s3h4 = sh.xpath("./th[4]")
                    s3h4 = s3h4[0].text_content().strip()
                    # print("detailed section3 heading col1", s3h4)

                except:
                    pass
            # print("section3")

            for s3b in section3body:
                s3b1 = s3b.xpath("./td[1]")
                s3b1 = s3b1[0].text_content().strip()
                # print("detailed section3 heading col1", s3b1)

                s3b2 = s3b.xpath("./td[2]")
                s3b2 = s3b2[0].text_content().strip()
                # print("detailed section3 heading col1", s3b2)

                try:
                    s3b3 = s3b.xpath("./td[3]")
                    s3b3 = s3b3[0].text_content().strip()
                    # print("detailed section3 heading col1", s3b3)
                except:
                    pass

                try:
                    s3b4 = s3b.xpath("./td[4]")
                    s3b4 = s3b4[0].text_content().strip()
                    # print("detailed section3 heading col1", s3b4)

                except:
                    pass

                try:
                    try:
                        s3 = {
                            s3h1: s3b1,
                            s3h2: s3b2,
                            s3h3: s3b3,
                            s3h4: s3b4
                        }
                    except:

                        s3 = {
                            s3h1: s3b1,
                            s3h2: s3b2,
                            s3h3: s3b3,
                        }
                except:

                    s3 = {
                        s3h1: s3b1,
                        s3h2: s3b2,

                    }

                section3_data.update({s3b1: s3})
                # print("section 3 detail",s3)
                if section3 == "Tender Technical Qualification Criteria":
                    tender_technical_qualification_criteria = section3_data.copy()
                    # print("Tender Technical Qualification Criteria:",tender_technical_qualification_criteria)

                elif section3 == "Document/Evidence Required from the Bidder":
                    document_evidence_required_from_the_bidder = section3_data.copy()
                    # print("Document/Evidence Required from the Bidder :",document_evidence_required_from_the_bidder)

                elif section3 == "Work Item Details":
                    work_item_details = section3_data.copy()
                    # print("Work Item Details:", work_item_details)

                elif section3 == "Technical Evaluation Criteria":
                    technical_evaluation_criteria = section3_data.copy()
                    # print("Technical Evaluation Criteria :", technical_evaluation_criteria)

                elif section3 == "Bill Of Quantity":
                    bill_of_quantity = section3_data.copy()
                    # print("Bill Of Quantity :", bill_of_quantity)

                elif section3 == "Delivery Schedules":
                    delivery_schedules = section3_data.copy()
                    # print("Delivery Schedules:", delivery_schedules)

                else:
                    pass

            # print("section3 information",section3_data)

            #here is the copy area

            try:
                s4={}
                section4_data = {}
                section4 = detailed_page_tree.xpath( "//span[ @ id = '_id29:useMe']/table/tbody/tr/td/table[2]/tbody/tr[18]")
                section4 = section4[0].text_content().strip()
                # print("section4 name is", section4)
                section4heading = detailed_page_tree.xpath( "//span[ @ id = '_id29:useMe']/table/tbody/tr/td/table[2]/tbody/tr[19]/td/table/thead/tr")
                section4body = detailed_page_tree.xpath( "//span[ @ id = '_id29:useMe']/table/tbody/tr/td/table[2]/tbody/tr[19]/td/table/tbody/tr")



                for s4h in section4heading:
                    s4h1 = s4h.xpath("./th[1]")
                    s4h1 = s4h1[0].text_content().strip()
                    # print("detailed section4 heading col1",s4h1)

                    s4h2 = s4h.xpath("./th[2]")
                    s4h2 = s4h2[0].text_content().strip()
                    # print("detailed section4 heading col2",s4h2)

                    try:
                        s4h3 = s4h.xpath("./th[3]")
                        s4h3 = s4h3[0].text_content().strip()
                        # print("detailed section4 heading col3",s4h3)
                    except:
                        pass

                    try:
                        s4h4 = s4h.xpath("./th[4]")
                        s4h4 = s4h4[0].text_content().strip()
                        # print("detailed section4 heading col4",s4h4)

                    except:
                        pass


                # print("section4")
                s4th={}
                for s4b in section4body:
                    s4b1 = s4b.xpath("./td[1]")
                    s4b1 = s4b1[0].text_content().strip()
                    # print("detailed section4 body col1",s4b1)

                    s4b2 = s4b.xpath("./td[2]")
                    s4b2 = s4b2[0].text_content().strip()
                    # print("detailed section4 body col2",s4b2)

                    try:
                        s4b3 = s4b.xpath("./td[3]")
                        s4b3 = s4b3[0].text_content().strip()
                        # print("detailed section4 body col3",s4b3)
                    except:
                        pass

                    try:
                        s4b4 = s4b.xpath("./td[4]")
                        s4b4 = s4b4[0].text_content().strip()
                        # print("detailed section4 body col4",s4b4)

                    except:
                        pass
                    try:
                        try:
                            s4th = {
                                s4h1: s4b1,
                                s4h2: s4b2,
                                s4h3: s4b3,
                                s4h4: s4b4
                            }
                        except:
                            s4th = {
                                s4h1: s4b1,
                                s4h2: s4b2,
                                s4h3: s4b3}
                    except:
                        s4th = {
                            s4h1: s4b1,
                            s4h2: s4b2,
                        }
                    section4_data.update({s4b1: s4th})

                    # print("section4 detail", s4th)
                    if section4 == "Tender Technical Qualification Criteria":
                        tender_technical_qualification_criteria = section4_data.copy()
                        # print("Tender Technical Qualification Criteria:", tender_technical_qualification_criteria)

                    elif section4 == "Document/Evidence Required from the Bidder":
                        document_evidence_required_from_the_bidder = section4_data.copy()
                        # print("Document/Evidence Required from the Bidder :", document_evidence_required_from_the_bidder)


                    elif section4 == "Work Item Details":
                        work_item_details = section4_data.copy()
                        # print("Work Item Details:", work_item_details)

                    elif section4 == "Technical Evaluation Criteria":
                        technical_evaluation_criteria = section4_data.copy()
                        # print("Technical Evaluation Criteria :", technical_evaluation_criteria)

                    elif section4 == "Bill Of Quantity":
                        bill_of_quantity = section4_data.copy()
                        # print("Bill Of Quantity :", bill_of_quantity)

                    elif section4 == "Delivery Schedules":
                        delivery_schedules = section4_data.copy()
                        # print("Delivery Schedules:", delivery_schedules)

                    else:
                        pass
                # print("section4 information", section4_data)
                try:
                    s5 = {}
                    section5_data={}
                    section5 = detailed_page_tree.xpath(
                        "//span[ @ id = '_id29:useMe']/table/tbody/tr/td/table[2]/tbody/tr[20]")
                    section5 = section5[0].text_content().strip()
                    # print("section5 name is", section5)
                    section5heading = detailed_page_tree.xpath(
                        "//span[ @ id = '_id29:useMe']/table/tbody/tr/td/table[2]/tbody/tr[21]/td/table/thead/tr")
                    section5body = detailed_page_tree.xpath(
                        "//span[ @ id = '_id29:useMe']/table/tbody/tr/td/table[2]/tbody/tr[21]/td/table/tbody/tr")

                    for s5h in section5heading:
                        s5h1 = s5h.xpath("./th[1]")
                        s5h1 = s5h1[0].text_content().strip()
                        # print("detailed section5 heading col1", s5h1)

                        s5h2 = s5h.xpath("./th[2]")
                        s5h2 = s5h2[0].text_content().strip()
                        # print("detailed section5 heading col2", s5h2)

                        try:
                            s5h3 = s5h.xpath("./th[3]")
                            s5h3 = s5h3[0].text_content().strip()
                            # print("detailed section5 heading col3", s5h3)
                        except:
                            pass

                        try:
                            s5h4 = s5h.xpath("./th[4]")
                            s5h4 = s5h4[0].text_content().strip()
                            # print("detailed section5 heading col4", s5h4)

                        except:
                            pass

                    # print("section5")

                    for s5b in section5body:
                        s5b1 = s5b.xpath("./td[1]")
                        s5b1 = s5b1[0].text_content().strip()
                        # print("detailed section5 body col1", s5b1)

                        s5b2 = s5b.xpath("./td[2]")
                        s5b2 = s5b2[0].text_content().strip()
                        # print("detailed section5 body col2", s5b2)

                        try:
                            s5b3 = s5b.xpath("./td[3]")
                            s5b3 = s5b3[0].text_content().strip()
                            # print("detailed section5 body col3", s5b3)
                        except:
                            pass

                        try:
                            s5b4 = s5b.xpath("./td[4]")
                            s5b4 = s5b4[0].text_content().strip()
                            # print("detailed section5 body col4", s5b4)

                        except:
                            pass

                        try:
                            s5b5 = s5b.xpath("./td[4]")
                            s5b5 = s5b5[0].text_content().strip()
                            # print("detailed section5 body col4", s5b4)

                        except:
                            pass
                        try:
                            try:
                                s5 = {
                                    s5h1: s5b1,
                                    s5h2: s5b2,
                                    s5h3: s5b3,
                                    s5h4: s5b4
                                }
                            except:
                                s5 = {
                                    s5h1: s5b1,
                                    s5h2: s5b2,
                                    s5h3: s5b3}
                        except:
                            s5 = {
                                s5h1: s5b1,
                                s5h2: s5b2
                            }

                        section5_data.update({s5b1: s5})
                        # print("section5 information", section5_data)
                        print("this is s5 dict", s5)
                        if section5 == "Tender Technical Qualification Criteria":
                            tender_technical_qualification_criteria = section5_data.copy()
                            # print("Tender Technical Qualification Criteria:", tender_technical_qualification_criteria)

                        elif section5 == "Document/Evidence Required from the Bidder":
                            document_evidence_required_from_the_bidder = section5_data.copy()
                            # print("Document/Evidence Required from the Bidder :",
                            #       document_evidence_required_from_the_bidder)


                        elif section5 == "Work Item Details":
                            work_item_details = section5_data.copy()
                            # print("Work Item Details:", work_item_details)

                        elif section5 == "Technical Evaluation Criteria":
                            technical_evaluation_criteria = section5_data.copy()
                            # print("Technical Evaluation Criteria :", technical_evaluation_criteria)

                        elif section5 == "Bill Of Quantity":
                            bill_of_quantity = section5_data.copy()
                            # print("Bill Of Quantity :", bill_of_quantity)

                        elif section5 == "Delivery Schedules":
                            delivery_schedules = section5_data.copy()
                            # print("Delivery Schedules:", delivery_schedules)

                        else:
                            pass

                    # print("section5body")
                    try:
                        s6={}
                        section6_data= {}
                        section6 = detailed_page_tree.xpath(
                            "//span[ @ id = '_id29:useMe']/table/tbody/tr/td/table[2]/tbody/tr[22]")
                        section6 = section6[0].text_content().strip()
                        # print("section6 name is", section6)
                        section6heading = detailed_page_tree.xpath(
                            "//span[ @ id = '_id29:useMe']/table/tbody/tr/td/table[2]/tbody/tr[23]/td/table/thead/tr")
                        section6body = detailed_page_tree.xpath(
                            "//span[ @ id = '_id29:useMe']/table/tbody/tr/td/table[2]/tbody/tr[23]/td/table/tbody/tr")

                        for s6h in section6heading:
                            s6h1 = s6h.xpath("./th[1]")
                            s6h1 = s6h1[0].text_content().strip()
                            # print("detailed section6 heading col1", s6h1)

                            s6h2 = s6h.xpath("./th[2]")
                            s6h2 = s6h2[0].text_content().strip()
                            # print("detailed section6 heading col2", s6h2)

                            try:
                                s6h3 = s6h.xpath("./th[3]")
                                s6h3 = s6h3[0].text_content().strip()
                                # print("detailed section6 heading col3", s6h3)
                            except:
                                pass

                            try:
                                s6h4 = s6h.xpath("./th[4]")
                                s6h4 = s6h4[0].text_content().strip()
                                # print("detailed section6 heading col4", s6h4)

                            except:
                                pass
                        # print("section6")

                        for s6b in section6body:
                            s6b1 = s6b.xpath("./td[1]")
                            s6b1 = s6b1[0].text_content().strip()
                            # print("detailed section6 body col1", s6b1)

                            s6b2 = s6b.xpath("./td[2]")
                            s6b2 = s6b2[0].text_content().strip()
                            # print("detailed section6 body col2", s6b2)

                            try:
                                s6b3 = s6b.xpath("./td[3]")
                                s6b3 = s6b3[0].text_content().strip()
                                # print("detailed section6 body col3", s6b3)
                            except:
                                pass

                            try:
                                s6b4 = s6b.xpath("./td[4]")
                                s6b4 = s6b4[0].text_content().strip()
                                # print("detailed section6 body col4", s6b4)

                            except:
                                pass

                            try:
                                s6b5 = s6b.xpath("./td[4]")
                                s6b5 = s6b5[0].text_content().strip()
                                # print("detailed section6 body col4", s6b4)

                            except:
                                pass

                            try:
                                try:
                                    s6 = {
                                        s6h1: s6b1,
                                        s6h2: s6b2,
                                        s6h3: s6b3,
                                        s6h4: s6b4
                                    }
                                except:
                                    s6 = {
                                        s6h1: s6b1,
                                        s6h2: s6b2,
                                        s6h3: s6b3}
                            except:
                                s6 = {
                                    s6h1: s6b1,
                                    s6h2: s6b2}
                            print("this is s6 dict", s6)
                            #
                            section6_data.update({s6b1: s6})
                            if section6 == "Tender Technical Qualification Criteria":
                                tender_technical_qualification_criteria = section6_data.copy()
                                # print("Tender Technical Qualification Criteria:", tender_technical_qualification_criteria)

                            elif section6 == "Document/Evidence Required from the Bidder":
                                document_evidence_required_from_the_bidder = section6_data.copy()
                                # print("Document/Evidence Required from the Bidder :",document_evidence_required_from_the_bidder)


                            elif section6 == "Work Item Details":
                                work_item_details = section6_data.copy()
                                # print("Work Item Details:", work_item_details)

                            elif section6 == "Technical Evaluation Criteria":
                                technical_evaluation_criteria = section6_data.copy()
                                # print("Technical Evaluation Criteria :", technical_evaluation_criteria)

                            elif section6 == "Bill Of Quantity":
                                bill_of_quantity = section6_data.copy()
                                # print("Bill Of Quantity :", bill_of_quantity)

                            elif section6 == "Delivery Schedules":
                                delivery_schedules = section6_data.copy()
                                # print("Delivery Schedules:", delivery_schedules)

                            else:
                                pass
                            # print("section6body")
                        # print("section6 data", section6_data)
                        try:
                            s7 = {}
                            section7_data = {}
                            #here is section7
                            section7 = detailed_page_tree.xpath(
                                "//span[ @ id = '_id29:useMe']/table/tbody/tr/td/table[2]/tbody/tr[24]")
                            section7heading = detailed_page_tree.xpath(
                                "//span[ @ id = '_id29:useMe']/table/tbody/tr/td/table[2]/tbody/tr[25]/td/table/thead/tr")
                            section7body = detailed_page_tree.xpath(
                                "//span[ @ id = '_id29:useMe']/table/tbody/tr/td/table[2]/tbody/tr[25]/td/table/tbody/tr")
                            for s7h in section7heading:
                                s7h1 = s7h.xpath("./th[1]")
                                s7h1 = s7h1[0].text_content().strip()
                                # print("detailed section7 heading col1", s7h1)

                                s7h2 = s7h.xpath("./th[2]")
                                s7h2 = s7h2[0].text_content().strip()
                                # print("detailed section7 heading col2", s7h2)

                                try:
                                    s7h3 = s7h.xpath("./th[3]")
                                    s7h3 = s7h3[0].text_content().strip()
                                    # print("detailed section7 heading col3", s7h3)
                                except:
                                    pass

                                try:
                                    s7h4 = s7h.xpath("./th[4]")
                                    s7h4 = s7h4[0].text_content().strip()
                                    # print("detailed section7 heading col4", s7h4)

                                except:
                                    pass
                            # print("section7")

                            for s7b in section7body:
                                s7b1 = s7b.xpath("./td[1]")
                                s7b1 = s7b1[0].text_content().strip()
                                # print("detailed section7 body col1", s7b1)

                                s7b2 = s7b.xpath("./td[2]")
                                s7b2 = s7b2[0].text_content().strip()
                                # print("detailed section7 body col2", s7b2)

                                try:
                                    s7b3 = s7b.xpath("./td[3]")
                                    s7b3 = s7b3[0].text_content().strip()
                                    # print("detailed section7 body col3", s7b3)
                                except:
                                    pass

                                try:
                                    s7b4 = s7b.xpath("./td[4]")
                                    s7b4 = s7b4[0].text_content().strip()
                                    # print("detailed section7 body col4", s7b4)

                                except:
                                    pass

                                try:
                                    s7b5 = s7b.xpath("./td[4]")
                                    s7b5 = s7b5[0].text_content().strip()
                                    # print("detailed section7 body col4", s7b4)

                                except:
                                    pass

                                try:
                                    try:
                                        s7 = {
                                            s7h1: s7b1,
                                            s7h2: s7b2,
                                            s7h3: s7b3,
                                            s7h4: s7b4
                                        }
                                    except:
                                        s7 = {
                                            s7h1: s7b1,
                                            s7h2: s7b2,
                                            s7h3: s7b3}
                                except:
                                    s7 = {
                                        s7h1: s7b1,
                                        s7h2: s7b2}
                                print("this is s7 dict", s7)

                                section7_data.update({s7b1: s7})
                                if section7 == "Tender Technical Qualification Criteria":
                                    tender_technical_qualification_criteria = section7_data.copy()
                                    print("Tender Technical Qualification Criteria:",tender_technical_qualification_criteria)

                                elif section7 == "Document/Evidence Required from the Bidder":
                                    document_evidence_required_from_the_bidder = section7_data.copy()
                                    print("Document/Evidence Required from the Bidder :",document_evidence_required_from_the_bidder)


                                elif section7 == "Work Item Details":
                                    work_item_details = section7_data.copy()
                                    print("Work Item Details:", work_item_details)

                                elif section7 == "Technical Evaluation Criteria":
                                    technical_evaluation_criteria = section7_data.copy()
                                    print("Technical Evaluation Criteria:", technical_evaluation_criteria)

                                elif section7 == "Bill Of Quantity":
                                    bill_of_quantity = section7_data.copy()
                                    print("Bill Of Quantity:", bill_of_quantity)

                                elif section7 == "Delivery Schedules":
                                    delivery_schedules = section7_data.copy()
                                    print("Delivery Schedules:",delivery_schedules)

                                else:
                                    pass
                                # print("section7body")
                            # print("section7 data", section7_data)

                            # print("section7 exist")




                            for i in range(18, 43):
                                try:

                                    try:
                                        col1 = detailed_page_tree.xpath(
                                            "//span[@id='_id29:useMe']/table/tbody/tr/td/table[2]/tbody/tr[" + str(
                                                i) + "]/td[1]")
                                        col1 = col1[0].text_content().strip()
                                        # print("row number is ", i, "col1 = ", col1)

                                        col2 = detailed_page_tree.xpath(
                                            "//span[@id='_id29:useMe']/table/tbody/tr/td/table[2]/tbody/tr[" + str(
                                                i) + "]/td[2]")
                                        col2 = col2[0].text_content().strip()
                                        # print("row number is ", i, "col2 = ", col2)
                                        try:
                                            rest_information = {
                                                col1: col2
                                            }
                                            # print("other details are", rest_information)
                                            rest_data_details.update({col1:rest_information})
                                        except:
                                            pass
                                    except:
                                        pass
                                except:
                                    pass

                            print("rest info is", rest_data_details)


                        except:
                            #section7 doesn't exist



                            for i in range(18, 43):
                                try:
                                    try:

                                        col1 = detailed_page_tree.xpath(
                                            "//span[@id='_id29:useMe']/table/tbody/tr/td/table[2]/tbody/tr["+str(i)+"]/td[1]")
                                        col1 = col1[0].text_content().strip()
                                        # print("row number is ", i, "col1 = ", col1)

                                        col2 = detailed_page_tree.xpath(
                                            "//span[@id='_id29:useMe']/table/tbody/tr/td/table[2]/tbody/tr["+str(i)+"]/td[2]")
                                        col2 = col2[0].text_content().strip()
                                        # print("row number is ", i, "col2 = ", col2)
                                        try:
                                            rest_information = {
                                                col1: col2
                                            }
                                            # print("other details are", rest_information)
                                            rest_data_details.update({col1 : rest_information})
                                        except:
                                            pass

                                    except:
                                        pass
                                except:
                                    pass
                            print("rest info is", rest_data_details)


                    except:


                        for i in range(18, 43):
                            try:
                                try:


                                    col1 = detailed_page_tree.xpath(
                                        "//span[@id='_id29:useMe']/table/tbody/tr/td/table[2]/tbody/tr["+str(i)+"]/td[1]")
                                    col1 = col1[0].text_content().strip()
                                    # print("row number is ", i, "col1 = ", col1)

                                    col2 = detailed_page_tree.xpath(
                                        "//span[@id='_id29:useMe']/table/tbody/tr/td/table[2]/tbody/tr["+str(i)+"]/td[2]")
                                    col2 = col2[0].text_content().strip()
                                    # print("row number is ", i, "col2 = ", col2)
                                    try:
                                        rest_information = {
                                            col1: col2
                                        }
                                        # print("other details are", rest_information)
                                        rest_data_details.update({col1 : rest_information})
                                    except:
                                        pass
                                except:
                                    pass


                            except:
                                pass
                        print("rest info is", rest_data_details)


                except:



                        for i in range(18, 43):
                            try:
                                try:

                                    col1 = detailed_page_tree.xpath(
                                        "//span[@id='_id29:useMe']/table/tbody/tr/td/table[2]/tbody/tr["+str(i)+"]/td[1]")
                                    col1 = col1[0].text_content().strip()
                                    # print("row number is ", i, "col1 = ", col1)

                                    col2 = detailed_page_tree.xpath(
                                        "//span[@id='_id29:useMe']/table/tbody/tr/td/table[2]/tbody/tr["+str(i)+"]/td[2]")
                                    col2 = col2[0].text_content().strip()
                                    # print("row number is ", i, "col2 = ", col2)
                                    try:

                                        rest_information = {
                                            col1: col2
                                        }
                                        # print("other details are", rest_information)
                                        rest_data_details.update({col1 : rest_information})
                                    except:
                                        pass
                                except:
                                    pass

                            except:
                                pass
                        print("rest info is", rest_data_details)

                    #section5 doesnt exist at all

                        # print("section4body")
                 # EXCEPT SECTION4 NOT EXIST
            except:



                    for i in range(18,43):
                        try:
                            try:

                               col1 = detailed_page_tree.xpath(
                                   "//span[@id='_id29:useMe']/table/tbody/tr/td/table[2]/tbody/tr["+str(i)+"]/td[1]")
                               col1 = col1[0].text_content().strip()
                               # print("row number is ",i,"col1 = ", col1)

                               col2 = detailed_page_tree.xpath(
                                   "//span[@id='_id29:useMe']/table/tbody/tr/td/table[2]/tbody/tr["+str(i)+"]/td[2]")
                               col2 = col2[0].text_content().strip()
                               # print("row number is ",i,"col2 = ", col2)
                               try:
                                   rest_information = {
                                       col1: col2
                                   }
                                   # print("other details are", rest_information)
                                   rest_data_details.update({col1 : rest_information})
                               except:
                                   pass
                            except:
                                pass
                        except:
                            pass

            print("tender detail of general conditions = ", general_conditions)
            print("tender detail of  work item = ", document_evidence_required_from_the_bidder)
            print("tender detail of  tender_technical_qualification_criteria = ", tender_technical_qualification_criteria)
            print("tender detail of  document_evidence_required_from_the_bidder = ",
                  document_evidence_required_from_the_bidder)
            print("tender detail of  technical_evaluation_criteria = ", technical_evaluation_criteria)
            print("tender detail of   bill_of_quantity = ", bill_of_quantity)
            print("tender detail of   delivery_schedules = ", delivery_schedules)

            print("all the other info of tender = ", rest_data_details)

            other_info_of_tender = {"other_info_of_tender": rest_data_details}
            general_conditions_data = {"general_conditions": general_conditions}

            work_item_details_data = {"work_item": work_item_details}
            tender_technical_qualification_criteria_data = {
                "tender_technical_qualification_criteria": tender_technical_qualification_criteria}
            technical_evaluation_criteria_data = {"technical_evaluation_criteria": technical_evaluation_criteria}
            bill_of_quantity_data = {"bill_of_quantity": bill_of_quantity}
            delivery_schedules_data = {"delivery_schedules": delivery_schedules}
            document_evidence_required_from_the_bidder_data = {
                "document_evidence_required_from_the_bidder": document_evidence_required_from_the_bidder}
            tender_data = {
                tender_no: (detailed_tender_info, general_conditions_data, tender_technical_qualification_criteria_data,
                            document_evidence_required_from_the_bidder_data, work_item_details_data,
                            technical_evaluation_criteria_data,
                            bill_of_quantity_data, delivery_schedules_data, other_info_of_tender)}

            alltenders.update({tender_no: tender_data})
            print("/n")
            print("complete detail of the tender", tender_data)

            back_link = driver.find_element_by_name('_id29:butBack')
            back_link.click()
            driver.implicitly_wait(10)
    next_button = driver.find_element(By.ID, "eprocTenders:dataScrollerIdnext")
    next_button.click()
    scroll_no+=1
print("/n")
print("/n")
print("detail of all the tenders ",alltenders)


    # for downloading the file




print("loading the download page")
flag=True

scroll_no = 0
# while flag == True:

#     driver.implicitly_wait(3)
#
#     page_tree = lxml.html.document_fromstring(driver.page_source)
#     list_of_rows = page_tree.xpath("//table/tbody[@id='eprocTenders:browserTableEprocTenders:tbody_element']/tr")
#
#     fresh_pg_first_sno_number = page_tree.xpath(
#         "//table/tbody[@id='eprocTenders:browserTableEprocTenders:tbody_element']/tr[1]/td[1]")
#     fresh_pg_first_sno_number = fresh_pg_first_sno_number[0].text_content()
#
#     if fresh_pg_first_sno_number== previous_pg_first_sno_number:
#         print("it stop coz of this")
#         flag=False
#     else:
#         pass
#
#     previous_pg_first_sno_number = fresh_pg_first_sno_number
#
#     try:
#         for row in list_of_rows:
#             tender_num = row.xpath("./td[3]")
#             tender_num = tender_num[0].text_content().strip()
#             print("tender num = " + str(tender_num))
#             k=1
#             download_page_button = driver.find_element_by_xpath(
#                 "//table/tbody[@id='eprocTenders:browserTableEprocTenders:tbody_element']/tr[" + str(k) + "]/td[11]/a[2]")
#             download_page_button.click()
#             download_page_tree = lxml.html.document_fromstring(driver.page_source)
#             download_section = download_page_tree.xpath(
#                 "/html/body/div[2]/table/tbody/tr[3]/td/form/table/tbody/tr/td/table[3]/tbody/tr")
#             len_of_download_section = len(download_section)
#             print("length is", len_of_download_section)
#
#             for d in download_section:
#                 file_number = 1
#                 download_sno = d.xpath("./td[1]")
#                 download_sno = int(download_sno[0].text_content())
#                 print("sno = " + str(download_sno))
#
#                 document_type = d.xpath("./td[2]")
#                 document_type = document_type[0].text_content().strip()
#                 print("document type = " + str(document_type))
#
#                 download_report_col = d.xpath("./td[3]")
#                 download_report_col = download_report_col[0].text_content()
#                 print("download report link text = " + str(download_report_col))
#
#                 download_data = {
#                     download_sno: (document_type, download_report_col)
#                 }
#
#                 print(download_data)
#                 # download_section_details.update({download_sno: download_data})
#
#
#
#                 file_number = file_number + 1
#
#                 download_section = {tender_num: download_data}
#                 alltenders.update({tender_num:download_section})
#
#             print()
#             print("download section of next tender")
#             k+=1
#             driver.implicitly_wait(7)
#             download_back_link = driver.find_element_by_name("_id29:butBack")
#             download_back_link.click()
#         print("download section details", download_section)
#     except:
#         print("no download page available")
#     print("whole data of all the  tenders is",alltenders)
#
#     print("start the downloading of files")
#     try:
#         for row in list_of_rows:
#             tender_num = row.xpath("./td[3]")
#             tender_num = tender_num[0].text_content().strip()
#             print("downloading the files of tender number = " + str(tender_num))
#             k=1
#             download_page_button = driver.find_element_by_xpath(
#                 "//table/tbody[@id='eprocTenders:browserTableEprocTenders:tbody_element']/tr[" + str(k) + "]/td[11]/a[2]")
#             download_page_button.click()
#             download_page_tree = lxml.html.document_fromstring(driver.page_source)
#             download_section = download_page_tree.xpath(
#                 "/html/body/div[2]/table/tbody/tr[3]/td/form/table/tbody/tr/td/table[3]/tbody/tr")
#             len_of_download_section = len(download_section)
#
#
#             for d in download_section:
#                 file_number = 1
#
#                 downloading_the_file = driver.find_element_by_xpath(
#                            "/html/body/div[2]/table/tbody/tr[3]/td/form/table/tbody/tr/td/table[3]/tbody/tr[" + str(file_number) + "]/td[3]/a")
#                 downloading_the_file.click()
#
#                 file_number = file_number + 1
#
#
#             k+=1
#             driver.implicitly_wait(7)
#             download_back_link = driver.find_element_by_name("_id29:butBack")
#             download_back_link.click()
#
#     except:
#         pass
#     next_button = driver.find_element(By.ID, "eprocTenders:dataScrollerIdnext")
#     next_button.click()
#     scroll_no += 1
# back_link = driver.find_element_by_name("_id29:butBack")
# back_link.click()

# while flag == True:
#
# #     page_tree = lxml.html.document_fromstring(driver.page_source)
#
# #     fresh_pg_first_sno_number = page_tree.xpath(
# #         "//table/tbody[@id='eprocTenders:browserTableEprocTenders:tbody_element']/tr[1]/td[1]")
# #     fresh_pg_first_sno_number = fresh_pg_first_sno_number[0].text_content()
# #
# #     if fresh_pg_first_sno_number == previous_pg_first_sno_number:
# #         print("it stop coz of this")
# #         flag = False
# #     else:
# #         pass
# #
# #     previous_pg_first_sno_number = fresh_pg_first_sno_number
# #
#
#
#     try:
#         for row in list_of_rows:
#             tender_num = row.xpath("./td[3]")
#             tender_num = tender_num[0].text_content().strip()
#
#
#             print("tender num = " + str(tender_num))
#
#             k = 1
#             download_page_button = driver.find_element_by_xpath(
#                 "//table/tbody[@id='eprocTenders:browserTableEprocTenders:tbody_element']/tr[" + str(
#                     k) + "]/td[11]/a[2]")
#             download_page_button.click()
#             download_page_tree = lxml.html.document_fromstring(driver.page_source)
#             download_section = download_page_tree.xpath(
#                 "/html/body/div[2]/table/tbody/tr[3]/td/form/table/tbody/tr/td/table[3]/tbody/tr")
#             len_of_download_section = len(download_section)
#             print("length is", len_of_download_section)
#
#             for d in download_section:
#                 file_number = 1
#                 download_sno = d.xpath("./td[1]")
#                 download_sno = int(download_sno[0].text_content())
#                 print("sno = " + str(download_sno))
#
#                 document_type = d.xpath("./td[2]")
#                 document_type = document_type[0].text_content().strip()
#                 print("document type = " + str(document_type))
#
#                 download_report_col = d.xpath("./td[3]")
#                 download_report_col = download_report_col[0].text_content()
#                 print("download report link text = " + str(download_report_col))
#
#                 download_data = {
#                     download_sno: (document_type, download_report_col)
#                 }
#
#                 print(download_data)
#                 # download_section_details.update({download_sno: download_data})
#
#                 file_number = file_number + 1
#
#                 download_section = {tender_num: download_data}
#                 alltenders.update({"download_section": download_section})
#
#             print()
#             print("download section of next tender")
#             k += 1
#             driver.implicitly_wait(7)
#             download_back_link = driver.find_element_by_name("_id29:butBack")
#             download_back_link.click()
#         print("download section details", download_section)
#     except:
#         print("no download page available")
#     print("whole data of tenders is", alltenders)
#
#     print("start the downloading of files")
#     try:
#         for row in list_of_rows:
#             tender_num = row.xpath("./td[3]")
#             tender_num = tender_num[0].text_content().strip()
#             print("downloading the files of tender number = " + str(tender_num))
#             k = 1
#             download_page_button = driver.find_element_by_xpath(
#                 "//table/tbody[@id='eprocTenders:browserTableEprocTenders:tbody_element']/tr[" + str(
#                     k) + "]/td[11]/a[2]")
#             download_page_button.click()
#             download_page_tree = lxml.html.document_fromstring(driver.page_source)
#             download_section = download_page_tree.xpath(
#                 "/html/body/div[2]/table/tbody/tr[3]/td/form/table/tbody/tr/td/table[3]/tbody/tr")
#             len_of_download_section = len(download_section)
#
#             for d in download_section:
#                 file_number = 1
#
#                 downloading_the_file = driver.find_element_by_xpath(
#                     "/html/body/div[2]/table/tbody/tr[3]/td/form/table/tbody/tr/td/table[3]/tbody/tr[" + str(
#                         file_number) + "]/td[3]/a")
#                 # downloading_the_file.click()
#
#                 file_number = file_number + 1
#
#             k += 1
#             driver.implicitly_wait(7)
#             download_back_link = driver.find_element_by_name("_id29:butBack")
#             download_back_link.click()
#     except:
#         pass







